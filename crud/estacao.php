<?php
/**
 * Projeto de aplicação CRUD utilizando PDO - Agenda de Contatos
 *https://alexandrebbarbosa.wordpress.com/2016/09/04/php-pdo-crud-completo/
 * Alexandre Bezerra Barbosa
 *http://localhost/teste/estacao.php
 */

// Verificar se foi enviando dados via POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id_estacao = (isset($_POST["id_estacao"]) && $_POST["id_estacao"] != null) ? $_POST["id_estacao"] : "";
    $nome_estacao = (isset($_POST["nome_estacao"]) && $_POST["nome_estacao"] != null) ? $_POST["nome_estacao"] : "";
    $longitude = (isset($_POST["longitude"]) && $_POST["longitude"] != null) ? $_POST["longitude"] : "";
    $latitude = (isset($_POST["latitude"]) && $_POST["latitude"] != null) ? $_POST["latitude"] : "";
    $ponto_de_referencia = (isset($_POST["ponto_de_referencia"]) && $_POST["ponto_de_referencia"] != null) ? $_POST["ponto_de_referencia"] : "";
} else if (!isset($id_estacao)) {
    // Se não se não foi setado nenhum valor para variável $id_estacao
    $id_estacao = (isset($_GET["id_estacao"]) && $_GET["id_estacao"] != null) ? $_GET["id_estacao"] : "";
    $nome_estacao = null;
    $longitude = null;
    $latitude = null;
    $ponto_de_referencia = null;
}

// Cria a conexão com o banco de dados
try {
    $conexao = new PDO("mysql:host=localhost;dbname=crudsimples", "root", "");
    $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conexao->exec("set names utf8");
} catch (PDOException $erro) {
    echo "Erro na conexão:".$erro->getMessage();
}

// Bloco If que Salva os dados no Banco - atua como Create e Update
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "save" && $nome_estacao != "") {
    try {
        if ($id_estacao != "") {
            $stmt = $conexao->prepare("UPDATE estacao SET nome_estacao=?, longitude=?, latitude=?, ponto_de_referencia=? WHERE id_estacao = ?");
            $stmt->bindParam(5, $id_estacao);
        } else {
            $stmt = $conexao->prepare("INSERT INTO estacao (nome_estacao, longitude, latitude, ponto_de_referencia) VALUES (?, ?, ?, ?)");
        }
        $stmt->bindParam(1, $nome_estacao);
        $stmt->bindParam(2, $longitude);
        $stmt->bindParam(3, $latitude);
        $stmt->bindParam(4, $ponto_de_referencia);

        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                echo "Dados cadastrados com sucesso!";
                $id_estacao = null;
                $nome_estacao = null;
                $longitude = null;
                $latitude = null;
                $ponto_de_referencia = null;
            } else {
                echo "Erro ao tentar efetivar cadastro";
            }
        } else {
            throw new PDOException("Erro: Não foi possível executar a declaração sql");
        }
    } catch (PDOException $erro) {
        echo "Erro: " . $erro->getMessage();
    }
}

// Bloco if que recupera as informações no formulário, etapa utilizada pelo Update
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "upd" && $id_estacao != "") {
    try {
        $stmt = $conexao->prepare("SELECT * FROM estacao WHERE id_estacao = ?");
        $stmt->bindParam(1, $id_estacao, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $rs = $stmt->fetch(PDO::FETCH_OBJ);
            $id_estacao = $rs->id_estacao;
            $nome_estacao = $rs->nome_estacao;
            $longitude = $rs->longitude;
            $latitude = $rs->latitude;
            $ponto_de_referencia = $rs->ponto_de_referencia;
        } else {
            throw new PDOException("Erro: Não foi possível executar a declaração sql");
        }
    } catch (PDOException $erro) {
        echo "Erro: " . $erro->getMessage();
    }
}

// Bloco if utilizado pela etapa Delete
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "del" && $id_estacao != "") {
    try {
        $stmt = $conexao->prepare("DELETE FROM estacao WHERE id_estacao = ?");
        $stmt->bindParam(1, $id_estacao, PDO::PARAM_INT);
        if ($stmt->execute()) {
            echo "Registo foi excluído com êxito";
            $id_estacao = null;
        } else {
            throw new PDOException("Erro: Não foi possível executar a declaração sql");
        }
    } catch (PDOException $erro) {
        echo "Erro: " . $erro->getMessage();
    }
}
?>
<!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Estacao</title>
            <link rel="stylesheet" href="css/bootstrap-grid.css">
            <link rel="stylesheet" href="css/bootstrap-grid.css.map">
            <link rel="stylesheet" href="css/bootstrap-grid.min.css">
            <link rel="stylesheet" href="css/bootstrap-grid.min.css.map">
            <link rel="stylesheet" href="css/bootstrap-reboot.css">
            <link rel="stylesheet" href="css/bootstrap-reboot.css.map">
            <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
            <link rel="stylesheet" href="css/bootstrap-reboot.min.css.map">
            <link rel="stylesheet" href="css/bootstrap.css">
            <link rel="stylesheet" href="css/bootstrap.css.map">
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <link rel="stylesheet" href="css/bootstrap.min.css.map">
            <link rel="stylesheet" href="css/test.css">
        </head>
        <body>
        <div class="grid-container">
        <nav class="item1 navbar navbar-expand-lg navbar-light bg-primary">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="#">Hidden brand</a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Disabled</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>

        </nav>
        <!--Tentando por menu lateral-->
        <div class="menuEE item2 container shadow-lg p-3 mb-5 bg-white rounded ">
            <h6 class="dropdown-header">Dropdown header</h6>
            <a class="dropdown-item" href="#">Usuário</a>
            <a class="dropdown-item" href="#">Torre</a>
        </div>
        <div class="item3 menuEE container shadow-lg p-3 mb-5 bg-white rounded ">
                <form action="?act=save" method="POST" class name="form1" >
                    <h1>CRUD Torres</h1>
                    <hr>
                    <input type="hidden" name="id" <?php
                    
                    // Preenche o id no campo id com um valor "value"
                    if (isset($id_estacao) && $id_estacao != null || $id_estacao != "") {
                        echo "value=\"{$id_estacao}\"";
                    }
                    ?> />
                    Nome:
                <input type="text" name="nome_estacao" <?php

                // Preenche o nome no campo nome com um valor "value"
                    if (isset($nome_estacao) && $nome_estacao != null || $nome_estacao != "") {
                        echo "value=\"{$nome_estacao}\"";
                    }
                    ?> />
                Longitude:
                <input type="text" name="longitude" <?php

                // Preenche o email no campo email com um valor "value"
                    if (isset($longitude) && $longitude != null || $longitude != "") {
                        echo "value=\"{$longitude}\"";
                    }
                    ?> />
                Latitude:
                <input type="text" name="latitude" <?php

                // Preenche o celular no campo celular com um valor "value"
                    if (isset($latitude) && $latitude != null || $latitude != "") {
                        echo "value=\"{$latitude}\"";
                    }
                    ?> />
                
                Ponto de Referencia:
                <input type="text" name="ponto_de_referencia" <?php

                // Preenche o celular no campo celular com um valor "value"
                    if (isset($ponto_de_referencia) && $ponto_de_referencia != null || $ponto_de_referencia != "") {
                        echo "value=\"{$ponto_de_referencia}\"";
                    }
                    ?> />
                
                <input type="submit" class="btn btn-info" value="salvar" />
                <input type="reset" class="btn btn-success" value="Novo" />
                <hr>
                </form>
                <table border="1" width="100%">
                    <tr>
                        <th>Nome</th>
                        <th>Longitude</th>
                        <th>Latitude</th>
                        <th>Ponto de Referencia</th>
                    </tr>
                    <?php

                    // Bloco que realiza o papel do Read - recupera os dados e apresenta na tela
                    try {
                        $stmt = $conexao->prepare("SELECT * FROM estacao");
                        if ($stmt->execute()) {
                            while ($rs = $stmt->fetch(PDO::FETCH_OBJ)) {
                                echo "<tr>";
                                echo "<td>" . $rs->nome_estacao . "</td><td>" . $rs->longitude . "</td><td>" . $rs->latitude . "</td><td>" . $rs->ponto_de_referencia
                                    . "</td><td><center><a href=\"?act=upd&id_estacao=" . $rs->id_estacao . "\">[Alterar]</a>"
                                    . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                    . "<a href=\"?act=del&id_estacao=" . $rs->id_estacao . "\">[Excluir]</a></center></td>";
                                echo "</tr>";
                            }
                        } else {
                            echo "Erro: Não foi possível recuperar os dados do banco de dados";
                        }
                    } catch (PDOException $erro) {
                        echo "Erro: " . $erro->getMessage();
                    }
                    ?>
                </table>
            <!--
            <p class="item3">aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
            <button id="button"> Editar </button> <button id="button"> Vizualizar </button> <button id="button"> Excluir </button>
            </p>
            -->
        </div>


        <div class="item5">

        
        </body>
    </html>