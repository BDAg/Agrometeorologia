<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id = (isset($_POST["id"]) && $_POST["id"] != null) ? $_POST["id"] : "";
    $nome = (isset($_POST["nome"]) && $_POST["nome"] != null) ? $_POST["nome"] : "";   
    $sobrenome = (isset($_POST["sobrenome"]) && $_POST["sobrenome"] != null) ? $_POST["sobrenome"] : "";
    $email = (isset($_POST["email"]) && $_POST["email"] != null) ? $_POST["email"] : "";
    $senha = (isset($_POST["senha"]) && $_POST["senha"] != null) ? $_POST["senha"] : "";
} else if (!isset($id)) {
    // Se não se não foi setado nenhum valor para variável $id
    $id = (isset($_GET["id"]) && $_GET["id"] != null) ? $_GET["id"] : "";
    $nome = null;
    $email = null;
    $sobrenome = null;
    $senha = null;
}

// Cria a conexão com o banco de dados
try {
    $conexao = new PDO("mysql:host=localhost;dbname=crudsimples", "root", "");
    $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conexao->exec("set names utf8");
} catch (PDOException $erro) {
    echo "Erro na conexão:".$erro->getMessage();
}

// Bloco If que Salva os dados no Banco - atua como Create e Update
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "save" && $id != "") {
    try {
        if ($id != "") {
            //Update
            $stmt = $conexao->prepare("UPDATE usuario SET nome=?, sobrenome=?, email=?, senha=? WHERE id = ?");
            $stmt->bindParam(5, $id);
        } else {
            //Create
            $stmt = $conexao->prepare("INSERT INTO usuario (nome, sobrenome, email, senha) VALUES (?, ?, ?, ?)");
        }
        $stmt->bindParam(1, $nome);
        $stmt->bindParam(2, $sobrenome);
        $stmt->bindParam(3, $email);
        $stmt->bindParam(4, $senha);

        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                echo "Dados cadastrados com sucesso!";
                $id = null;
                $nome = null;
                $sobrenome = null;
                $email = null;
                $senha = null;
            } else {
                echo "Erro ao tentar efetivar cadastro";
            }
        } else {
            throw new PDOException("Erro: Não foi possível executar a declaração sql");
        }
    } catch (PDOException $erro) {
        echo "Erro: " . $erro->getMessage();
    }
}

// Bloco if que recupera as informações no formulário, etapa utilizada pelo Update
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "upd" && $id != "") {
    try {
        $stmt = $conexao->prepare("SELECT * FROM usuario WHERE id = ?");
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $rs = $stmt->fetch(PDO::FETCH_OBJ);
            $id = $rs->id;
            $nome = $rs->nome;
            $email = $rs->email;
            $sobrenome = $rs->sobrenome;
            $senha = $rs->senha;
        } else {
            throw new PDOException("Erro: Não foi possível executar a declaração sql");
        }
    } catch (PDOException $erro) {
        echo "Erro: " . $erro->getMessage();
    }
}

// Bloco if utilizado pela etapa Delete
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "del" && $id != "") {
    try {
        $stmt = $conexao->prepare("DELETE FROM usuario WHERE id = ?");
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            echo "Registo foi excluído com êxito";
            $id = null;
        } else {
            throw new PDOException("Erro: Não foi possível executar a declaração sql");
        }
    } catch (PDOException $erro) {
        echo "Erro: " . $erro->getMessage();
    }
}
?>
<!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>usuario</title>
            <link rel="stylesheet" href="css/bootstrap-grid.css">
            <link rel="stylesheet" href="css/bootstrap-grid.css.map">
            <link rel="stylesheet" href="css/bootstrap-grid.min.css">
            <link rel="stylesheet" href="css/bootstrap-grid.min.css.map">
            <link rel="stylesheet" href="css/bootstrap-reboot.css">
            <link rel="stylesheet" href="css/bootstrap-reboot.css.map">
            <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
            <link rel="stylesheet" href="css/bootstrap-reboot.min.css.map">
            <link rel="stylesheet" href="css/bootstrap.css">
            <link rel="stylesheet" href="css/bootstrap.css.map">
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <link rel="stylesheet" href="css/bootstrap.min.css.map">
            <link rel="stylesheet" href="css/test.css">
        </head>
        <body>
        <div class="grid-container">
        <nav class="item1 navbar navbar-expand-lg navbar-light bg-primary">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="#">Hidden brand</a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Disabled</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>

        </nav>
        <!--Tentando por menu lateral-->
        <div class="menuEE item2 container shadow-lg p-3 mb-5 bg-white rounded ">
            <h6 class="dropdown-header">Dropdown header</h6>
            <a class="dropdown-item" href="#">Usuário</a>
            <a class="dropdown-item" href="#">Torre</a>
        </div>
        <div class="item3 menuEE container shadow-lg p-3 mb-5 bg-white rounded ">
                <form action="?act=save" method="POST" class name="form1" >
                    <h1>CRUD Torres</h1>
                    <hr>
                    <input type="hidden" name="id" <?php
                    
                    // Preenche o id no campo id com um valor "value"
                    if (isset($id) && $id != null || $id != "") {
                        echo "value=\"{$id}\"";
                    }
                    ?> />
                    Nome:
                <input type="text" name="nome" class="form-control" id="exampleFormControlInput1" <?php

                // Preenche o nome no campo nome com um valor "value"
                    if (isset($nome) && $nome != null || $nome != "") {
                        echo "value=\"{$nome}\"";
                    }
                    ?> />
                sobrenome:
                    <input type="text" name="sobrenome" class="form-control" id="exampleFormControlInput1" <?php

                // Preenche o celular no campo celular com um valor "value"
                    if (isset($sobrenome) && $sobrenome != null || $sobrenome != "") {
                        echo "value=\"{$sobrenome}\"";
                    }
                    ?> />
                email:
                <input type="email" name="email" class="form-control" id="exampleFormControlInput1" <?php

                // Preenche o email no campo email com um valor "value"
                    if (isset($email) && $email != null || $email != "") {
                        echo "value=\"{$email}\"";
                    }
                    ?> />
                
                senha:
                <input type="password" name="senha" class="form-control" id="exampleFormControlInput1" <?php

                // Preenche o celular no campo celular com um valor "value"
                    if (isset($senha) && $senha != null || $senha != "") {
                        echo "value=\"{$senha}\"";
                    }
                    ?> />
                
                <input type="submit"  class="btn btn-info" value="salvar" />
                <a href="cadastro_usuario.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Novo</a>
                <hr>
                </form>
                <table border="1" width="100%">
                    <tr>
                        <th>Nome</th>
                        <th>sobrenome</th>
                        <th>email</th>
                        <th>senha</th>
                    </tr>
                    <?php

                    // Bloco que realiza o papel do Read - recupera os dados e apresenta na tela
                    try {
                        $stmt = $conexao->prepare("SELECT * FROM usuario");
                        if ($stmt->execute()) {
                            while ($rs = $stmt->fetch(PDO::FETCH_OBJ)) {
                                echo "<tr>";
                                echo "<td>" . $rs->nome . "</td><td>" . $rs->sobrenome . "</td><td>" . $rs->email
                                    . "</td><td><center><a href=\"?act=upd&id=" . $rs->id . "\">[Alterar]</a>"
                                    . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                    . "<a href=\"?act=del&id=" . $rs->id . "\">[Excluir]</a></center></td>";
                                echo "</tr>";
                            }
                        } else {
                            echo "Erro: Não foi possível recuperar os dados do banco de dados";
                        }
                    } catch (PDOException $erro) {
                        echo "Erro: " . $erro->getMessage();
                    }
                    ?>
                </table>

        </div>

        <div class="item5">
                </body>
    </html>
