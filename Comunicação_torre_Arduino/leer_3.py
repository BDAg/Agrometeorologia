import mysql.connector
from mysql.connector import errorcode

# Obtain connection string information from the portal
config = {
  'host':'mydemoserver.mysql.database.azure.com',
  'user':'myadmin@mydemoserver',
  'password':'yourpassword',
  'database':'quickstartdb'
}

arq = open('F:/log.txt', 'r')
texto = arq.readlines() 

# Construct connection string
try:
   conn = mysql.connector.connect(**config)
   print("Connection established")
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with the user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)
else:
  cursor = conn.cursor()
  cursor.execute("SELECT MAX(id) FROM inventory;")
  id = cursor.fetchall()

  for linha in texto :
    id += 1
    
#   ler_linha(linha)
    valores = linha.split()
    hora                 = str(valores[0])
    data                 = str(valores[1])
    velocidade_do_vento  = str(valores[2])
    pluviometro          = str(valores[3])
    temperatura          = str(valores[4])
    umidade              = str(valores[5])
    sentido_do_vento     = str(valores[6])  
    # Drop previous table of same name if one exists
    cursor.execute("DROP TABLE IF EXISTS inventory;")
    print("Finished dropping table (if existed).")

    # Create table
    cursor.execute("CREATE TABLE inventory (id serial PRIMARY KEY, name VARCHAR(50), quantity INTEGER);")
    print("Finished creating table.")

    # Insert some data into table
    cursor.execute("INSERT INTO inventory (id_dados, hora, data,velocidade_do_vento, pluviometro, temperatura, umidade,  sentido_do_vento, estacao_id_estacao) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s);", (id, hora, data, velocidade_do_vento, pluviometro, temperatura, umidade,  sentido_do_vento, id_estacao))
    print("Inserted",cursor.rowcount,"row(s) of data.")
    
  # Cleanup
  conn.commit()
  cursor.close()
  conn.close()
  print("Done.")