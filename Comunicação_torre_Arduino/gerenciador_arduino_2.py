 Carrega as bibliotecas
#!/usr/bin/python
# -*- coding: utf-8 -*- 
import MySQLdb
import time


db = MySQLdb.connect(host="localhost", user="root", passwd="", db="Agrometeorologia")
cursor = db.cursor()

# Informacoes iniciais
print ("*** Lendo os valores de temperatura e umidade");

while(1):
   # Efetua a leitura do sensor
   umid, temp = Adafruit_DHT.read_retry(sensor, pino_sensor);
   # Caso leitura esteja ok, mostra os valores na tela
   if umid is not None and temp is not None:
     print ("Temperatura = {0:0.1f}  Umidade = {1:0.1f}\n").format(temp, umid);
     cursor.execute("""INSERT INTO temperatura (temp, umidade) VALUES ('temp', 'umid')""")
     db.commit()
     print ("Aguarda 5 segundos para efetuar nova leitura...\n");
     time.sleep(5)
   else:
     # Mensagem de erro de comunicacao com o sensor
     print("Falha ao ler dados do DHT11 !!!")