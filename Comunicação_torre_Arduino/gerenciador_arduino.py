#!/usr/bin/python3

from serial import Serial
import pymysql 
import times
from datetime import datetime
from time import gmtime , strftime

arduino = Serial('COM7', 9600)

now = datetime.now()

# Open database connection
db = pymysql.connect(host='127.0.0.1', unix_socket='/tmp/mysql.sock', user='root', passwd="", db='Agrometeorologia')

# prepare a cursor object using cursor() method
cursor = db.cursor()

# execute SQL query using execute() method.
cursor.execute("SELECT VERSION()")

# Fetch a single row using fetchone() method.
data = cursor.fetchone()
print ("Database version : %s " % data)


while True:
    arduino = arduino.readline()
    # prepare a cursor object using cursor() method
    cursor = db.cursor()
    id = int(12)
    # execute SQL query using execute() method.
    cursor.execute("SELECT VERSION()")
    d = strftime ( "%d %m %Y" , gmtime ())
    h = strftime ( "%H:%M:%S" , gmtime ())

    # Fetch a single row using fetchone() method.
    data = cursor.fetchone()
    print ("Database version : %s " % data)
    cursor.execute("INSERT INTO dados(hora, data,velocidade_do_vento, pluviometro, temperatura, umidade,  sentido_do_vento, estacao_id_estacao) VALUES(%s, %s, %s, %s,)" % (h, d, arduino, id))
    con.commit()