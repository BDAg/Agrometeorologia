CREATE SCHEMA IF NOT EXISTS `Agrometeorologia` DEFAULT CHARACTER SET utf8 ;
USE `Agrometeorologia` ;

-- -----------------------------------------------------
-- Table `usuario`
-- -----------------------------------------------------
CREATE TABLE `usuario` (
  `id_usuario` INT NOT NULL,
  `nome` VARCHAR(100) NULL,
  `email` VARCHAR(100) NULL,
  `senha` VARCHAR(100) NULL,
  PRIMARY KEY (`id_usuario`));


-- -----------------------------------------------------
-- Table `estacao`
-- -----------------------------------------------------
CREATE TABLE `estacao` (
  `id_estacao` INT NOT NULL,
  `nome_estacao` VARCHAR(45) NOT NULL,
  `longitude` VARCHAR(45) NOT NULL,
  `latitude` VARCHAR(45) NOT NULL,
  `ponto_de referencia` VARCHAR(400) NULL,
  `usuario_id_usuario` INT NOT NULL,
  PRIMARY KEY (`id_estacao`),
  INDEX `fk_estacao_usuario_idx` (`usuario_id_usuario` ASC),
  CONSTRAINT `fk_estacao_usuario`
    FOREIGN KEY (`usuario_id_usuario`)
    REFERENCES `usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `dados`
-- -----------------------------------------------------
CREATE TABLE `dados` (
  `id_dados` INT NOT NULL,
  `hora` VARCHAR(45) NULL,
  `Data` VARCHAR(45) NULL,
  `temperatura` VARCHAR(45) NULL,
  `umidade` VARCHAR(45) NULL,
  `velocidade_do_vento` VARCHAR(45) NULL,
  `sentido_do_vento` VARCHAR(45) NULL,
  `pluviometro` VARCHAR(45) NULL,
  `estacao_id_estacao` INT NOT NULL,
  PRIMARY KEY (`id_dados`),
  INDEX `fk_dados_estacao1_idx` (`estacao_id_estacao` ASC),
  CONSTRAINT `fk_dados_estacao1`
    FOREIGN KEY (`estacao_id_estacao`)
    REFERENCES `estacao` (`id_estacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


SELECT * FROM usuario;
SELECT * FROM estacao;
SELECT * FROM dados;

DROP DATABASE Agrometeorologia;
