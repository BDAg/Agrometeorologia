import mysql.connector
from mysql.connector import errorcode

 #Obtain connection string information from the portal
config = {
  'host':'NomeBanco.mysql.database.azure.com',
  'user':'rootNomeBanco@NomeBanco',
  'password':'',
  'database':'NomeDataBase',
#  ssl_ca={'keyssl.crt.pem'},
#  ssl_verify_cert=true)
  }


arq = open('LOG.TXT', 'r')
texto = arq.readlines()




try:
   conn = mysql.connector.connect(**config)
   print("Connection established")
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with the user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)
else:
  cursor = conn.cursor()
  cursor.execute("DROP TABLE IF EXISTS inventory;")
  #id = cursor.fetchall()
  id = 0
  for linha in texto :
    id += 1
    print("id", id)
    #print(texto)
    print('Iniciando a leitura de Dados')
#   ler_linha(linha)
    valores = linha.split()
    #print('Valor',valores)
    hora                 = str(valores[0])
    data                 = str(valores[1])
    velocidade_do_vento  = str(valores[2])
    pluviometro          = str(valores[3])
    temperatura          = str(valores[4])
    umidade              = str(valores[5])
    sentido_do_vento     = str(valores[6])  
    # Drop previous table of same name if one exists
    #cursor.execute("DROP TABLE IF EXISTS inventory;")
    #print("Finished dropping table (if existed).")

    # Create table
    #cursor.execute("CREATE TABLE inventory (id serial PRIMARY KEY, name VARCHAR(50), quantity INTEGER);")
    #print("Finished creating table.")

    # Insert some data into table
    cursor.execute("INSERT INTO dados (Data, Hora, Temeratura, Umidade, Vento_velocidade, Direcao_vento, Mm_chuva, Estacao_idEstacao) VALUES (%s, %s, %s, %s, %s, %s, %s, %s);", (data, hora, temperatura, umidade, velocidade_do_vento, sentido_do_vento, pluviometro, 0))
    print("Inserted",cursor.rowcount,"row(s) of data.")
    
  # Cleanup
  conn.commit()
  cursor.close()
  conn.close()
  print("Done.")
  #cnx.close()