// Anenometro para Estação Meteorológica
// Versão 1.0
// 13.09.2018
// Autor: Johan Rafhael Maehashi

// Inclusão das bibliotecas
//#include "DTH.h" // Biblioteca do DHT
#include <DHT_U.h>
#include <DHT.h>

//Constantes
const float pi = 3.14159265;    //Numero Pi
int period = 5000;              //tempo de medida (miliseconds)
int delaytime = 2000;           //Intervalor entre amostras
int radius = 147;               //Raio do anemometro (mm)

const int pluv = 5;             //pluviometro ligado na porta digital 4

// Variaveis

//Anemometro
unsigned int Sample  = 0;        //Armazena o número de amostras
unsigned int counter = 0;        //Contador para o sensor  
unsigned int RPM = 0;            //Rotações por minuto
float speedms = 0;               //Velocidade do vento (m/s)
float speedkm = 0;               //Velocidade do vento (km/h)
int porta_anemotro = 2;           //Porta do anemometro 

//Pluviometro
int val = 0;                     // Valor da leitura do pluviometro
int oldVal = 0;                  // Valor anterior da leitura do pluviometro
int countVal = 0;                // Contador de pulsos do pluviometro
double mmRain = 0;               // chuva em milimetros

//Teperatura e Umidade

// Definição das constantes
#define DHTPIN 4 // Pino digital 4 para leitura dos dados
#define DHTTYPE DHT22 // Define sensor como DHT22

DHT dht(DHTPIN, DHTTYPE); // Configuração do DHT

// Configurações Iniciais
void setup() {
  pinMode(porta_anemotro, INPUT);       // configura o digital 2 como entrada
  digitalWrite(porta_anemotro, HIGH);   // internall pull-up active

  pinMode(pluv, INPUT_PULLUP);          // inicializa o pino do arduino para o pluviometro com pull_up

  // Inicializando o DHT 22
  dht.begin();

  Serial.begin(9600);            // inicia serial em 9600 baud rate
 
}
// Loop
void loop() {
  anemometro();
  pluviometro();
  temp();
  delay(delaytime);
}
///////////////////////////////////////////////////////////////////////////////////
//Anemometro

void anemometro(){
  Sample++;
  Serial.print(Sample);
  Serial.print(": Start mensurement...");
  windvelocity();
  Serial.println("    finished.");
  Serial.print("Counter: ");
  Serial.print(counter);
  Serial.print("; RPM: ");
  Serial.print(RPM);
  Serial.print("; Velocidade: ");  
  Serial.print(speedms);
  Serial.print(" [m/s] ");
  Serial.print(speedkm);
  Serial.print(" [km/h] ");
  Serial.println();
}

// Função para medir a velocidade do vento
void windvelocity(){
  speedms = 0;
  speedkm = 0;

  counter = 0;
  attachInterrupt(0, addcount, RISING);
  unsigned long millis();
  long startTime = millis();
  while(millis() < startTime + period) {}
  RPMcalc();
  WindSpeed();
  
}

// Calcular RPM
void RPMcalc(){
  RPM = ((counter)*60)/(period/1000); //Calcular rotacoes por minuto
}

// Calcula velocidade do vento
void WindSpeed(){
  speedms = ((4*pi*radius*RPM)/60) / 1000; //velocidade do vento em m/s
  speedkm = speedms*3.6; // velocidade do vento em km/h
}


// Incrementa contador
void addcount(){
  counter++;
}

///////////////////////////////////////////////////////////////////////////////////

//Pluviometro
void pluviometro(){
  // leitura do estado do pluviometro
  val = digitalRead(pluv);

  // verifica se houve mudança no estado
  if ((val == LOW) && (oldVal == HIGH)){
    delay(10); //espera para balanço
    countVal = countVal + 1; // incrementa contagem de pulsos
    mmRain = countVal * 0.25; // cada pulso representa 0.25mm de chuva
    oldVal = val; // valor de val passa para oldVal

    
    Serial.print("Contagem (Pulsos): ");
    Serial.print(countVal);
    Serial.print(" - ");
    Serial.print("Chuva (mm): ");
    Serial.print(mmRain);
    Serial.println(".");
    
  } else {
    oldVal = val; //caso não tenha mudança de estado, faça nada
  }
}

///////////////////////////////////////////////////////////////////////////////////

//Temperatura e Umidade

void temp(){
  float h = dht.readHumidity(); // leitura e armazenamento da umidade
  float t = dht.readTemperature(); // leitura e armazenamento da temperatura

  // Checar se a leitura falhou
  if (isnan(h) || isnan(t)){
    Serial.println("Falha na Leitura!");
    return;
  }
  Serial.print("Umidade: ");
  Serial.print(h);
  Serial.print(" - ");
  Serial.print("Temperatura: ");
  Serial.print(t);
  Serial.println(".");
}
