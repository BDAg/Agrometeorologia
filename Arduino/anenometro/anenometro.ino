// Anenometro para Estação Meteorológica
// Versão 1.0
// 15.08.2018
// Autor: Ricardo Favan

//Constantes
const float pi = 3.14159265;    //Numero Pi
int period = 5000;              //tempo de medida (miliseconds)
int delaytime = 2000;           //Intervalor entre amostras
int radius = 147;               //Raio do anemometro (mm)

// Variaveis Globais
unsigned int Sample  = 0;        //Armazena o número de amostras
unsigned int counter = 0;        //Contador para o sensor  
unsigned int RPM = 0;            //Rotações por minuto
float speedms = 0;             //Velocidade do vento (m/s)
float speedkm = 0;             //Velocidade do vento (km/h)


// Configurações Iniciais
void setup() {
  pinMode(10, INPUT);            // configura o digital 2 como entrada
  digitalWrite(10, HIGH);        // internall pull-up active

  Serial.begin(9600);           // inicia serial em 9600 baud rate

}
// Loop
void loop() {
  Sample++;
  Serial.print(Sample);
  Serial.print(": Start mensurement...");
  windvelocity();
  Serial.println("    finished.");
  Serial.print("Counter: ");
  Serial.print(counter);
  Serial.print("; RPM: ");
  Serial.print(RPM);
  Serial.print("; Velocidade: ");  
  Serial.print(speedms);
  Serial.print(" [m/s] ");
  Serial.print(speedkm);
  Serial.print(" [km/h] ");
  Serial.println();

  delay(delaytime);            // taxa de atualização
  
}

// Função para medir a velocidade do vento
void windvelocity(){
  speedms = 0;
  speedkm = 0;

  counter = 0;
  attachInterrupt(0, addcount, RISING);
  unsigned long millis();
  long startTime = millis();
  while(millis() < startTime + period) {}
  RPMcalc();
  WindSpeed();
  
}

// Calcular RPM
void RPMcalc(){
  RPM = ((counter)*60)/(period/1000); //Calcular rotacoes por minuto
}

// Calcula velocidade do vento
void WindSpeed(){
  speedms = ((4*pi*radius*RPM)/60) / 1000; //velocidade do vento em m/s
  speedkm = speedms*3.6; // velocidade do vento em km/h
}


// Incrementa contador
void addcount(){
  counter++;
}





