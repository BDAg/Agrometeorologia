// Anenometro para Estação Meteorológica
// Versão 2.0
// 24.10.2018
// Autor: Johan Rafhael Maehashi

// Inclusão das bibliotecas
//#include "DTH.h" 
// Biblioteca do DHT
#include <DHT.h>
#include <SPI.h>
#include <SD.h>
#include <stdlib.h>
#include <DS1307.h>

/*
Anemometro
Resistor de 330
PINO 2

Direção do vento
Resistor de 4k7
PINO 3

DHT22 
PINO 6

Pluviometro
PINO 5
RESISTOR 7K

Cartão SD
CS   10
SDK  13
MOSI 11
MISO 12
VCC 3.3


Relogio  
SCL A5
SDA A4
*/


//Constantes
const float pi = 3.14159265;    //Numero Pi
int period = 5000;              //tempo de medida (miliseconds)
int delaytime = 2000;           //Intervalor entre amostras
int radius = 147;               //Raio do anemometro (mm)



// Variaveis
///////////////////////////////////////////////////////////////////////////////////
//Anemometro
unsigned int Sample  = 0;        //Armazena o número de amostras
unsigned int counter = 0;        //Contador para o sensor  
unsigned int RPM = 0;            //Rotações por minuto
float speedms = 0;               //Velocidade do vento (m/s)
float speedkm = 0;               //Velocidade do vento (km/h)
int porta_anemotro = 2;           //Porta do anemometro 

///////////////////////////////////////////////////////////////////////////////////
//Direção
// Definição das Variaveis Globais
int pin=3; //Sensor ligado na porta analogica 3
float valor; //armazena o valor lido no sensor
int gr=0; //graus de rotação da haste em relacao ao Norte
String dir; //direção da haste

////////////////////////////////////////////////////////////////////////////////////////////////////
//Pluviometro
int val = 0;                     // Valor da leitura do pluviometro
int oldVal = 0;                  // Valor anterior da leitura do pluviometro
int countVal = 0;                // Contador de pulsos do pluviometro
double mmRain = 0;               // chuva em milimetros
const int pluv = 5;             //pluviometro ligado na porta digital 5
int vala = 0;
//////////////////////////////////////////////////////////////////////////////////////////////
//Teperatura e Umidade

// Definição das constantes
#define DHTPIN 6 // Pino digital 6 para leitura dos dados
#define DHTTYPE DHT22 // Define sensor como DHT22
float t;
float h;

DHT dht(DHTPIN, DHTTYPE); // Configuração do DHT

//////////////////////////////////////////////////////////////////////////////////////////////
//Variaveis para cartao SD
const int CS = 4;
char dataString[7];
File meuArquivo;

//Variaveis para sensor ultrassonico
int PinTrigger = 3;//Pino usado para disparar os pulsos do sensor
int PinEcho = 2;//pino usado para ler a saida do sensor
float TempoEcho=0;
const float VelocidadeSom_mpors = 340;//em metros por segundo
const float VelocidadeSom_mporus = 0.000340;//em metros por microsegundo

///////////////////////////////////////////////////////////////////////////////////
//Relogio
DS1307 rtc(A4, A5);

//------------------------------SetUp---------------------------------------------//
// Configurações Iniciais
void setup() {
  //Aciona o relogio
  rtc.halt(false);
   
  //As linhas abaixo setam a data e hora do modulo
  //e podem ser comentada apos a primeira utilizacao
  rtc.setDOW(FRIDAY);      //Define o dia da semana
  rtc.setTime(11, 28, 0);     //Define o horario
  rtc.setDate(9, 11, 2018);   //Define o dia, mes e ano
   
  //Definicoes do pino SQW/Out
  rtc.setSQWRate(SQW_RATE_1);
  rtc.enableSQW(true);

  pinMode(porta_anemotro, INPUT);       // configura o digital 2 como entrada
  digitalWrite(porta_anemotro, HIGH);   // internall pull-up active

  pinMode(pluv, INPUT_PULLUP);          // inicializa o pino do arduino para o pluviometro com pull_up

  // Inicializando o DHT 22
  dht.begin();

  //Cartão SD
  pinMode(CS, OUTPUT);
  Serial.print("Inicializando cartao SD...");
  if (!SD.begin(CS)) 
  {
    Serial.println("Falha na Inicializacao!");
    return;
  }
  Serial.println("Inicializacao terminada");


  Serial.begin(9600);            // inicia serial em 9600 baud rate

 
}


//----------------------------------Loop--------------------------------//


// Loop
void loop() {

  if (meuArquivo = SD.open("log.txt",FILE_WRITE)) 
  {
    Serial.println("Gravou");
    //Escreve no cartao SD por meio do objeto meuArquivo 
    meuArquivo.println(dataString);
    anemometro();
    pluviometro();
    temp();
    direcao();
    meuArquivo.close();
  } 
  else 
  {
    // se o arquivo nao abrir, mostra msg de erro
    Serial.println("Erro ao abrir log.txt");
  }

  
  delay(delaytime);
  
}
///////////////////////////////////////////////////////////////////////////////////
//Relogio
void relogio(){
  //Mostra as informações no Serial Monitor
  Serial.print("Hora : ");
  Serial.print(rtc.getTimeStr());
  meuArquivo.print(rtc.getTimeStr());
  meuArquivo.print(" ");
  Serial.print(", ");
  Serial.print("Data : ");
  Serial.print(rtc.getDateStr(FORMAT_SHORT));
  meuArquivo.print(rtc.getDateStr(FORMAT_SHORT));
  meuArquivo.print(" ");
  Serial.print(", ");
   
  //Aguarda 1 segundo e repete o processo
  delay (1000);
}
///////////////////////////////////////////////////////////////////////////////////
//Anemometro

void anemometro(){
  Sample++;
  //Serial.print(Sample);
  //Serial.print(": Start mensurement...");
  windvelocity();
  //Serial.println("    finished.");
  //Serial.print("Counter: ");
  //Serial.print(counter);
  //Serial.print("; RPM: ");
  //Serial.print(RPM);
  //Serial.print("; Velocidade: ");  
  //Serial.print(speedms);
  //Serial.print(" [m/s] ");
  Serial.print(speedkm);
  meuArquivo.print(speedkm);
  //Serial.print(" [km/h] ");
  Serial.print(", ");
  meuArquivo.print(" ");
  //Serial.println();
}

// Função para medir a velocidade do vento
void windvelocity(){
  speedms = 0;
  speedkm = 0;

  counter = 0;
  attachInterrupt(0, addcount, RISING);
  unsigned long millis();
  long startTime = millis();
  while(millis() < startTime + period) {}
  RPMcalc();
  WindSpeed();
  
}

// Calcular RPM
void RPMcalc(){
  RPM = ((counter)*60)/(period/1000); //Calcular rotacoes por minuto
}

// Calcula velocidade do vento
void WindSpeed(){
  speedms = ((4*pi*radius*RPM)/60) / 1000; //velocidade do vento em m/s
  speedkm = speedms*3.6; // velocidade do vento em km/h
}


// Incrementa contador
void addcount(){
  counter++;
}

///////////////////////////////////////////////////////////////////////////////////

//Pluviometro
void pluviometro(){
  // leitura do estado do pluviometro
  val = digitalRead(pluv);

  // verifica se houve mudança no estado
  if ((val == LOW) && (oldVal == HIGH)){
    //delay(10); //espera para balanço
    countVal = countVal + 1; // incrementa contagem de pulsos
    mmRain = countVal * 0.25; // cada pulso representa 0.25mm de chuva
    oldVal = val; // valor de val passa para oldVal

    
    //Serial.print("Contagem (Pulsos): ");
    //Serial.print(countVal);
    //Serial.print(" - ");
    //Serial.print("Chuva (mm): ");
    Serial.print(mmRain);
    meuArquivo.print(mmRain);
    //Serial.print("mm");
    Serial.print(", ");
    meuArquivo.print(" ");
    vala = 0;
    
  } else {
    Serial.print("0");
    meuArquivo.print("0");
    //Serial.print("mm");
    Serial.print(", ");
    meuArquivo.print(" ");
    oldVal = val; //caso não tenha mudança de estado, faça nada
  }
}

///////////////////////////////////////////////////////////////////////////////////

//Temperatura e Umidade

void temp(){
  float h = dht.readHumidity(); // leitura e armazenamento da umidade
  float t = dht.readTemperature(); // leitura e armazenamento da temperatura

  // Checar se a leitura falhou
  if (isnan(h) || isnan(t)){
    Serial.print("Falha na Leitura!");
    meuArquivo.print("Falha na Leitura!");
    Serial.print(", ");
    meuArquivo.print(" ");
    Serial.print("Falha na Leitura!");
    meuArquivo.print("Falha na Leitura!");
    Serial.print(", ");
    meuArquivo.print(" ");
    return;
  }
  //Serial.print("Umidade: ");
  Serial.print(h);
  meuArquivo.print(h);
  //Serial.print("%");
  Serial.print(", ");
  meuArquivo.print(" ");
  //Serial.print("Temperatura: ");
  Serial.print(t);
  meuArquivo.print(t);
  //Serial.print("c");
  Serial.print(", ");
  meuArquivo.print(" ");
}

/////////////////////////////////////////////////////////////////////////////////////

//Direção do vento 

void direcao(){
  valor = analogRead(pin); //Leitura do valor do sensor

  // Define a direção da haste e seu grau de rotação em relação ao norte
  if(valor <= 115){ 
    gr = 135;
    dir = "SE";
  }else if(valor <= 130){
    gr = 90;
    dir = "E";  
  }else if(valor <= 145){
    gr = 45;
    dir = "NE";
  }else if(valor <= 170){
    gr = 0;
    dir = "N";
  }else if(valor <= 210){
    gr = 315;
    dir = "NW";
  }else if(valor <= 260){
    gr = 270;
    dir = "W";
  }else if(valor <= 340){
    gr = 225;
    dir = "SW";
  }else{
    gr = 180;
    dir = "S";
  }

  // Imprime no Monitor Serial os valores lidos
  Serial.print("Valor: ");
  Serial.print(valor);
  //Serial.print(" - Direcao: ");
  //Serial.print(gr);
  //Serial.print(" graus");
  //Serial.print(" - Referência: ");
  Serial.println(dir);
  meuArquivo.println(dir);
  meuArquivo.close();
  //Serial.println(", ");
}
