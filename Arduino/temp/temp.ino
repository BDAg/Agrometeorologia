

// Programa: Temperatura e Umidade (leitura dos dados - estação meteorologica)
// Versão: 1.0
// Autor: Fabricio e Geisa
// Data: 22.08.2018

// Inclusão das bibliotecas
//#include "DTH.h" // Biblioteca do DHT
#include <DHT_U.h>
#include <DHT.h>


// Definição das constantes
#define DHTPIN 4 // Pino digital 4 para leitura dos dados
#define DHTTYPE DHT22 // Define sensor como DHT22

DHT dht(DHTPIN, DHTTYPE); // Configuração do DHT

void setup() {
  // Inicializando o DHT 22
  dht.begin();
  // Inicializando o Serial
  Serial.begin(9600);
  Serial.println("DHT22 test!");

}

void loop() {
  delay(1000); //Espera 1 segundo para cada leitura
  float h = dht.readHumidity(); // leitura e armazenamento da umidade
  float t = dht.readTemperature(); // leitura e armazenamento da temperatura

  // Checar se a leitura falhou
  if (isnan(h) || isnan(t)){
    Serial.println("Falha na Leitura!");
    return;
  }
  Serial.print("Umidade: ");
  Serial.print(h);
  Serial.print(" - ");
  Serial.print("Temperatura: ");
  Serial.print(t);
  Serial.println(".");

}
