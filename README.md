# Agrometeorologia

Este projeto tem o objetivo de coletar e armazenar dados climaticos de Pompéia-SP.

## Documentos do projeto 

### Primeira entrega 29/08/2018

* [Project Charter](https://gitlab.com/BDAg/Agrometeorologia/wikis/Project_charter)
* [Cronograma](https://gitlab.com/BDAg/Agrometeorologia/wikis/Cronograma)
* [Equipe](https://gitlab.com/BDAg/Agrometeorologia/wikis/Equipe)
* [Mapa de Conhecimento](https://gitlab.com/BDAg/Agrometeorologia/wikis/Mind_Map)
* [Matriz de Habilidades](https://gitlab.com/BDAg/Agrometeorologia/wikis/Matriz_de_Habilidades_da_Equipe)

### Segunda entrega 13/09/2018

* [MVP]()
* [Arquitetura da solução](https://gitlab.com/BDAg/Agrometeorologia/wikis/arquitetura-de-solução)
* [Modelagem de dados](https://gitlab.com/BDAg/Agrometeorologia/wikis/modelagem_dados)
* [Mapa de definição tecnologica](https://gitlab.com/BDAg/Agrometeorologia/wikis/mapa_def_tec)
* [Montagem do ambiente de desenvolvimento]()
* [Lista de Funcionalidades](https://gitlab.com/BDAg/Agrometeorologia/wikis/lista_funcionalidade)
 
### Terceira entrega 23/09/2018

* [Implementação Sensor de Temperatura e Umidade](https://gitlab.com/BDAg/Agrometeorologia/blob/master/temp.ino)
* [Pluviômetro](https://gitlab.com/BDAg/Agrometeorologia/blob/master/pluviometro.ino)
* [Anemômetro](https://gitlab.com/BDAg/Agrometeorologia/blob/master/anenometro.ino)
* [Definição Sensores e Hardware]("")
* [Teste de Validação]("")
* [Teste Coleta de Dados]("")
* [Implementação da CRON de coleta de dados e armazenamento local]("")

### Quarta Entrega 10/10/2018

* [Diagrama de Telas]("")
* [Montagem do Banco de Dados]("")
* [CRUD de Usuário]("")
* [CRUD de Estação]("")
* [Alterar Senha]("")
* [Recuperação Senha por E-mail]("")
* [Teste de Usuário](https://gitlab.com/BDAg/Agrometeorologia/blob/master/Documentos/TESTE_CRUD_USU%C3%81RIO.pdf)
* [Teste de Estação](https://gitlab.com/BDAg/Agrometeorologia/blob/master/Documentos/Teste_de_Esta%C3%A7%C3%A3o.pdf)
 
### Quinta Entrega 17/10/2018

* [Implementação do serviço de transmissão de dados]("")
* [WEB - Serviço de Armazenamento e Consulta de Dados Meteorológicos]("")
* [WEB - Mapa das Estações](https://gitlab.com/BDAg/Agrometeorologia/blob/master/pages/mapaestacao.html)
* [WEB - Relatório por estação(Gráficos)]("")
* [WEB - Filtro por Data]("")
* [Estudo de Usuabilidade dos Sistema(UX)]("")
* [Teste de Coleta de Dados]("")
* [Teste de Relatório](”https://gitlab.com/BDAg/Agrometeorologia/blob/master/teste_de_relatório.pdf”)
* [Teste Integrado da Solução e Correções]("")

### Sexta Entrega 05/11/2018

* [Montagem Infraestrutura da NUVEM]("")
* [Documentação do Ambiente de Produção]("")
* [Deploy do Projeto em Produção]("")
* [Teste em Ambiente de Produção]("")

### Fechamento do Projeto

* [Documentação no GIT]("")
* [Apresentação do Projeto]("")
* [Lições Aprendidas]("")
