 
<!DOCTYPE html>
<html lang="pt-br">
  <head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clima Show - FATEC Shunji Nishimura</title>
    <meta name="description" content="Estação Meteorológica não oficial, destinada a ajudar pessoas de Pompéia e Região">
    <meta name="keywords" content="Fatec, FATEC, Fatec Shunji Nishimura, Agronomia, Meteorológica, Meteorologia, Pompéia, Marília">
    <meta name="robots" content="index, follow">
    <meta name="author" content="Agrometeorologia BDAG">
    <link rel="stylesheet" href="css/cadastro.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link rel="icon" href="img/logo.png">
  
  </head>
  
  <body>
      <div>
          <img src = "img/logo.png"  width="12%" height="12%" >
        </div>
        <form action="php_action/create.php" method="post">    
          <div>
          <label for="name">Nome:</label>
          <input type="text" name="name" id="name" />
      </div>
      <div>
        <label for="snome">Sobrenome:</label>
        <input type="text" name="snome" id="snome" />
    </div>
      <div>
          <label for="email">E-mail:</label>
          <input type="email" name="email" id="email" />
      </div>

      <div>
        <label for="senha">Senha:</label>
        <input type="password" name="senha" id="senha" />
      </div>
      <div>
        <label for="confSenha">Confirmar senha:</label>
        <input type="password" name="confSenha" id="confSenha" />
     </div>
      <div>
          <tr>
      <td><button type="submit">Save Changes</button></td>
    <td><a href="index2.php"><button type="button">Back</button></a></td>
    </tr>
      </div>
    
  </form>

  </body>
  
  