
<?php
// Verificar se foi enviando dados via POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $nome = (isset($_POST["nome"]) && $_POST["nome"] != null) ? $_POST["nome"] : "";
    $Snome = (isset($_POST["sobrenome"]) && $_POST["sobrenome"] != null) ? $_POST["sobrenome"] : "";
    $email = (isset($_POST["email"]) && $_POST["email"] != null) ? $_POST["email"] : "";
    $senha = (isset($_POST["senha"]) && $_POST["senha"] != null) ? $_POST["senha"] : NULL;
} else if (!isset($id)) {
    // Se não se não foi setado nenhum valor para variável $id
    $nome = NULL;
    $Snome = NULL;
    $email = NULL;
    $senha = NULL;
}
 
try {
    $conexao = new PDO("mysql:host=localhost; dbname=mydb", "pi", "pi");
    $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conexao->exec("set names utf8");
} catch (PDOException $erro) {
    echo "Erro na conexão:" . $erro->getMessage();
}
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "save" && $nome != "") {
    try {
        $stmt = $conexao->prepare("INSERT INTO Usuarios (Nome, Sobrenome, Email, Senha) VALUES (?, ?, ?,?)");
        $stmt->bindParam(1, $nome);
        $stmt->bindParam(2, $Snome);
        $stmt->bindParam(3, $email);
        $stmt->bindParam(4, $senha);
         
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                echo "Dado  s cadastrados com sucesso!";
                $nome = null;
                $Snome = null;
                $email = null;
                $senha = null;
            } else {
                echo "Erro ao tentar efetivar cadastro";
            }
        } else {
               throw new PDOException("Erro: Não foi possível executar a declaração sql");
        }
    } catch (PDOException $erro) {
        echo "Erro: " . $erro->getMessage();
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clima Show - FATEC Shunji Nishimura</title>
    <meta name="description" content="Estação Meteorológica não oficial, destinada a ajudar pessoas de Pompéia e Região">
    <meta name="keywords" content="Fatec, FATEC, Fatec Shunji Nishimura, Agronomia, Meteorológica, Meteorologia, Pompéia, Marília">
    <meta name="robots" content="index, follow">
    <meta name="author" content="Agrometeorologia BDAG">
    <link rel="stylesheet" href="css/cadastro.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link rel="icon" href="img/logo.png">
  
  </head>
  
  <body>
      <div>
          <img src = "img/logo.png"  width="12%" height="12%" >
        </div>
    <form action="?act=save" method="post" name="form1">
      <div>
          <label for="nome">Nome:</label>
          <input type="text" id="nome"<?php
            // Preenche o nome no campo nome com um valor "value"
            if (isset($nome) && $nome != null || $nome != ""){
                echo "value=\"{$nome}\"";
            }
            ?> />
      </div>
      <div>
          <label for="nome">Sobrenome:</label>
          <input type="text" id="sobrenome"<?php
            // Preenche o nome no campo nome com um valor "value"
            if (isset($Snome) && $Snome != null || $Snome != ""){
                echo "value=\"{$Snome}\"";
            }
            ?> />
      </div>
      <div>
          <label for="email">E-mail:</label>
          <input type="email" id="email" <?php
            // Preenche o email no campo email com um valor "value"
            if (isset($email) && $email != null || $email != ""){
                echo "value=\"{$email}\"";
            }
            ?> />
      </div>
      <div>
        <label for="senha">Senha:</label>
        <input type="password" id="senha" <?php
            // Preenche a senha no campo senha com um valor "value"
            if (isset($senha) && $senha != null || $senha != ""){
                echo "value=\"{$senha}\"";
            }
            ?> />
      </div>
      <div>
        <label for="confSenha">Confirmar senha:</label>
        <input type="password" id="confSenha" />
     </div>
      <div>
        <input type="submit" value="salvar" >
        <input type="reset" value="Novo">
      </div>
    
  </form>

  </body>
