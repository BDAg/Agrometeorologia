import pymysql.cursors
from time import gmtime , strftime
from Serial import serial
import serial


# Connect to the database
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='',
                             db='Agrometeorologia',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

arduino = serial.Serial('COM7', 9600)


with True:
    arduino = arduino.readline()
    id = int(12)
    d = strftime ( "%d %m %Y" , gmtime ())
    h = strftime ( "%H:%M:%S" , gmtime ())

    # Create a new record
    sql = "INSERT INTO `dados` (hora, data,velocidade_do_vento, pluviometro, temperatura, umidade,  sentido_do_vento, estacao_id_estacao) VALUES(%s, %s, %s, %s,)"
    cursor.execute(sql, (h, d, arduino, id))    

# connection is not autocommit by default. So you must commit to save
# your changes.
connection.commit()

#    with connection.cursor() as cursor:
#        # Read a single record
#        sql = "SELECT `id`, `password` FROM `users` WHERE `email`=%s"
#        cursor.execute(sql, ('webmaster@python.org',))
#        result = cursor.fetchone()
#        print(result)